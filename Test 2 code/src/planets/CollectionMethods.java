package planets;

import java.util.ArrayList;
import java.util.Collection;


public class CollectionMethods {
	
	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size){
		
		Collection<Planet> validPlanets = new ArrayList<Planet>();
		
		for(Planet planet: planets) {
			if(((Planet) planets).getRadius()>=size){
				validPlanets.add(planet);
				
	
			}
			
		}
		return validPlanets;	
	}

}
