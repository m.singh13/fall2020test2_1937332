package planets;
/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet> {
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	/**
	 * 
	 * @return True if two planets are equal, false if they are not
	 */
	
	public boolean equals(Object obj) {
		Planet otherPlanet = (Planet) obj;
		if(this.name.equals(otherPlanet.name)&&this.planetarySystemName.equals(otherPlanet.planetarySystemName)) {
			return true;
		}
		return false;
		
	}
	/**
	 * @return hashCode based on the name + planetarySystemName
	 */
	
	public int hashCode() {
		String combined = this.name + this.planetarySystemName;
		return combined.hashCode();
	}
	
	/**
	 * @param otherPlanet representing the senting planet
	 * @return true if both books are equal, helps sort the Array
	 */
	
	public int compareTo(Planet otherPlanet) {
		int nameComparison = this.getName().compareTo(otherPlanet.getName());
		if(nameComparison ==0) {
			return (int)(this.radius - otherPlanet.radius);
		}
		return nameComparison;
		
	}
	
}
