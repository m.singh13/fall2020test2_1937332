package workers;
/**
 * 
 * @author Mandeep Singh
 *
 * This class extends to HourlyEmployee and calculates the weekly salary for overtime employees (if they work overtime)
 */

public class UnionizedHourlyEmployee extends HourlyEmployee {
	//private fields
	private int maxHoursPerWeek;
	private double overtimeRate;
	//constructors
	public UnionizedHourlyEmployee(int hoursPerWeek, double hourlyPay,int maxHoursPerWeek, double overtimeRate) {
		super(hoursPerWeek,hourlyPay);
		this.maxHoursPerWeek = maxHoursPerWeek;
		this.overtimeRate = overtimeRate;
	}
	//getWeeklyPay
	public double getWeeklyPay() {
		
		double weeklyPay = 0.0;
		
		if(getHoursPerWeek() <= maxHoursPerWeek) {
			weeklyPay= (getHoursPerWeek()*getHourlyPay());
		}
		
		else if(getHoursPerWeek()>maxHoursPerWeek) {
			int extraHours = getHoursPerWeek() - maxHoursPerWeek;
			weeklyPay=((maxHoursPerWeek*getHourlyPay()) +(getHourlyPay()*overtimeRate*extraHours));
			
		}
		return weeklyPay;
	}

}
