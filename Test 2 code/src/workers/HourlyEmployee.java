package workers;
/**
 * 
 * @author Mandeep Singh
 * 
 * This class implements the Employee and calculates the weekly salary by the hoursPerWeek and hourlyPay
 *
 */

public class HourlyEmployee implements Employee {
	//Private fields
	private int hoursPerWeek;
	private double hourlyPay;
	//Constructors
	public HourlyEmployee (int hoursPerWeek, double hourlyPay) {
		
		this.hoursPerWeek = hoursPerWeek;
		this.hourlyPay = hourlyPay;
	}
	//getters
	public double getHourlyPay() {return this.hourlyPay;}
	public int getHoursPerWeek() {return this.hoursPerWeek;}
	public double getWeeklyPay() {return this.hourlyPay*this.hoursPerWeek;}
	

}
