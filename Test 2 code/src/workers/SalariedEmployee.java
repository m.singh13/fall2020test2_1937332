package workers;
/**
 * 
 * @author Mandeep Singh 
 * 
 * This class implements the Employee and calculates the weekly salary for all employees that have a yearly salary
 *
 */

public class SalariedEmployee implements Employee {
	//Private fields
	private double yearlySalary;
	//Constructors
	public SalariedEmployee(double yearlySalary){this.yearlySalary = yearlySalary;}
	//getters
	public double getyearlySalary(){return this.yearlySalary;}
	public double getWeeklyPay() {return this.yearlySalary/52;}

}
