package workers;
/**
 * 
 * @author Mandeep Singh
 * 
 * This class is the main application, it is used for creating each employee and calculates the totalExpenses per week for all employees
 *
 */
public class PayrollManagement {

	public static void main(String[] args) {
		
		Employee[] employees = new Employee [5];
		
		employees[0] = new SalariedEmployee(30000.00);
		employees[1] = new SalariedEmployee(50000.00);
		employees[2] = new HourlyEmployee(40,12.50);
		employees[3] = new HourlyEmployee(90,50.00);
		employees[4] = new UnionizedHourlyEmployee(30,15.00,15,1.8);
		
		double totalExpenses = getTotalExpenses(employees);
		
		System.out.println("Total Expenses: "+totalExpenses);
		
	}
	/**
	 * @param employees  An Array of Employee representing each type of employees salary
	 * @return A double representing the totalExpenses per week for all employees
	 */
	public static double getTotalExpenses(Employee[] employees) {
		double totalExpenses =0.0;
	for(int i =0; i<employees.length;i++) {
		totalExpenses += employees[i].getWeeklyPay();
	}
	return totalExpenses;	
	}

}
