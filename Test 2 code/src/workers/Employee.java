package workers;

/**
 * 
 * @author Mandeep Singh
 * 
 * This class is an Employee interface that makes sure every type of employee includes a getWeeklyPay method
 *
 */

public interface Employee {
	
	double getWeeklyPay();
}
